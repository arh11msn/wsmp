package main

import "net/url"
import "github.com/gorilla/websocket"

import "bitbucket.org/arh11msn/golang/wsmp/wsmp-api"

type MainServerClient struct {
    conn *websocket.Conn
}

func NewMainServerClient() *MainServerClient {
    msclient := MainServerClient{}
    return &msclient
}

func (msclient *MainServerClient) DefaultConnect() {
    adr := url.URL{Scheme: "ws", Host: wsmp.LOCALHOST+":"+wsmp.DEFAULT_PORT, Path: "/appserver/"}
    conn, _, err := websocket.DefaultDialer.Dial(adr.String(), nil)
    if err != nil {
        panic(err)
    }
    msclient.conn = conn
}
