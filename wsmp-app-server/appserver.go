package main

import "log"

type AppServer struct {
    msclient *MainServerClient
}

var Server *AppServer = nil

func newAppServer() *AppServer {

    appserver := AppServer{}
    
    appserver.msclient = NewMainServerClient()
    
    return &appserver
}

func GetAppServer() *AppServer {
    if Server == nil {
        Server = newAppServer()
    }
    return Server
}

func (appserver *AppServer) Start() {

    log.Print("AppServer is starting...")
    
    appserver.msclient.DefaultConnect()
}

func StartAppServer() {
    server := GetAppServer()
    server.Start()
}
