package main

import "log"

import "bitbucket.org/arh11msn/golang/wsmp/wsmp-api"

type Settings struct {
    port string
}

func NewSettings() *Settings {
    settings := Settings{}
    return &settings
}

func (settings *Settings) Load() error {
    log.Printf("    Loading settings...")
    if true {
        log.Print("    ",SETTINGS_LOAD_ERROR)
        return SETTINGS_LOAD_ERROR
    }
    return nil
}

func (settings *Settings) Save() error {
    return nil
}

func (settings *Settings) SetDefault() {
    log.Printf("    Use dafault settings...")
    settings.port = wsmp.DEFAULT_PORT
}
