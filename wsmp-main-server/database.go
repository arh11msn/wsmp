package main

import "log"

import "gopkg.in/mgo.v2"
//import "gopkg.in/mgo.v2/bson"
import "bitbucket.org/arh11msn/golang/wsmp/wsmp-api"

const (
    MONGO_SESSION_NAME = "WSMP_MAIN_SERVER"
)

type DataBase struct {
    mongo *mgo.Session
    users *mgo.Collection
}

func NewDataBase() *DataBase {
    database := DataBase{}
    return &database
}

func (database *DataBase) Connect() {
    log.Printf("    Connecting to database %s...", wsmp.LOCALHOST)
    connect, err := mgo.Dial(wsmp.LOCALHOST)
    if err != nil {
        panic(err)
    }
    database.mongo = connect
    log.Printf("    Successfully connected!")
}
