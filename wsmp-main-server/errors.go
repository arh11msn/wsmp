package main

import "errors"

var (
    SETTINGS_LOAD_ERROR = errors.New("Can not load settings!")
)
