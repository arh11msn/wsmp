package main

import "log"

import "time"

import "net/http"

import "github.com/gorilla/mux"
import "github.com/gorilla/websocket"
import "github.com/gorilla/sessions"


type MainServer struct {
    settings *Settings
    database *DataBase
    router *mux.Router
    store *sessions.CookieStore
    upgrader *websocket.Upgrader
}

var Server *MainServer = nil

func newMainServer() *MainServer {

    mainserver := MainServer{}
    
    mainserver.database = NewDataBase()
    
    mainserver.settings = NewSettings()
    
    mainserver.router = mux.NewRouter()
    mainserver.store = sessions.NewCookieStore([]byte(""))
    mainserver.upgrader = &websocket.Upgrader{}
    
    return &mainserver
}

func GetMainServer() *MainServer {
    if Server == nil {
        Server = newMainServer()
    }
    return Server
}

func (mainserver *MainServer) Start() {
    
    log.Print("Server is starting...")
    
    mainserver.database.Connect()
    
    err := mainserver.settings.Load()
    if err != nil {
        mainserver.settings.SetDefault()
    }
    
    r := mainserver.router
    r.HandleFunc("/appserver/", AppServerHandler).Methods("GET")
    
    http.Handle("/", r)
    log.Printf("Listen and Serve on port %s!", mainserver.settings.port)
    go func() {
        time.Sleep(1. * time.Second)
        log.Printf("Server was started!")
    }()
    log.Fatal(http.ListenAndServe(":" + mainserver.settings.port, nil))
}

func StartMainServer() {
    server := GetMainServer()
    server.Start()
}
